# Ubuntu Touch devices

The new (new) [Ubuntu Touch devices website](https://devices.ubuntu-touch.io)! Made using [Astro](https://astro.build/). You can start a development server by running `npm run dev`.

## Documentation
Documentation of the device file format is available directly on the [website](https://devices.ubuntu-touch.io/about/device-file).

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Build status
[![Netlify Status](https://api.netlify.com/api/v1/badges/ab82fe1e-1ee0-44a0-b932-d89b43d72167/deploy-status)](https://app.netlify.com/sites/ubports-ubuntu-touch-devices/deploys)
