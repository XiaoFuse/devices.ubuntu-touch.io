---
name: "Samsung Galaxy Note 10 Plus (Exynos)"
deviceType: "phone"
description: "Samsung's flagship for 2019. Features a Exynos 9825 SOC."
price:
  avg: 700

deviceInfo:
  - id: "cpu"
    value: "2.7GHz Octa Core (Exynos)"
  - id: "chipset"
    value: "Samsung Exynos 9825"
  - id: "gpu"
    value: "Mali-G76 MP12"
  - id: "rom"
    value: "256/512GB"
  - id: "ram"
    value: "12GB"
  - id: "android"
    value: "LineageOS 18.1"
  - id: "battery"
    value: "4300 mAh"
  - id: "display"
    value: "1440x3040 pixels(Quad HD+), 6.3 in"
  - id: "rearCamera"
    value: "12MP"
  - id: "frontCamera"
    value: "10MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "162.3 mm (6.39 in) x 77.2 mm (3.04 in) x 7.9 mm (0.31 in)"
  - id: "weight"
    value: "196 g"
  - id: "releaseDate"
    value: "23 August 2019"

contributors:
  - name: Sharath Chandra Sreenidhi Chellinki
    role: "Maintainer"
    renewals:
      - "2022-10-20"
    forum: "https://forums.ubports.com/user/chellinki"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-8671/8671-profileavatar-1664267370216.png"

sources:
  portType: "community"
  portPath: "android11"
  deviceGroup: "samsung-galaxy-note-10-plus"
  deviceSource: "samsung-exynos9825"
  kernelSource: "kernel-samsung-exynos9825"
  showDevicePipelines: true

communityHelp:
  - name: "Telegram group"
    link: "https://t.me/utbeyondxlte"
---
