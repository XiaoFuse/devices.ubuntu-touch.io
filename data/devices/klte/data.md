---
name: "Samsung Galaxy S5"
deviceType: "phone"
description: "This phone is the last high-end Samsung phone from which you can replace the battery without any tools!"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 2.5 GHz Krait 400"
  - id: "chipset"
    value: "Qualcomm MSM8974AC Snapdragon 801"
  - id: "gpu"
    value: "Adreno 330"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "2GB"
  - id: "android"
    value: "Android 4.4.2 (KitKat), upgradable to 6.0 (Marshmallow)"
  - id: "battery"
    value: "2800 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.1 in"
  - id: "rearCamera"
    value: "16MP"
  - id: "frontCamera"
    value: "2MP"
contributors:
  - name: RealTheHexagon
    forum: https://forums.ubports.com/user/thehexagon
    github: https://github.com/RealTheHexagon
    gitlab: https://gitlab.com/RealTheHexagon
---
